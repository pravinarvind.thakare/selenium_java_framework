# Selenium_Java_Framework

## About Selenium

Selenium is open source automation testing tool,but technically it is open source library used to automate GUI.
selenium ui automation is used for usability and functionality testing of an application.
selenium automates web based application.
selnium architecture has four componants:

1.Selenium client Library-inthis user writes code in their respective programming language like java,python,ruby etc.
2.JSON wired protocol over HTTP-it is an ope statndard that provides atransport mechanism for transferring data between client and server on the web .
3.browserr drivers-selenium browser drivers are native to each browser,interacting with browseer by establishing a secure connection.Slenium supports different browser drivers such as ChromeDriver,EdgeDriver,GeckoDriver,SafariDriver and InterneteExplorereDriver.
4.Browsers-selenium provides support for browsers like chrome ,firefox,safari,internet explorer,edge etc.

## Componants of project

* Firstly,created differnt common functions such as common methods repositery for different UI common       utilities ,methods for instantiating browser and launching the URL,common methods for different utility like  taking screenshot and creating log directory.
* used Actions class for operating keyboard and mouse.
* implemented auto suggestive dropdown,calender,exception handling,opening multiple tabs 
* implemented windows handles for switching driver control to different tabs.
*implemented POM design texnique 
*Automated E2E product buying process for Snapdeal e-commerce website using POM implementation.
*Used Allure library to generate user friendly reports.
