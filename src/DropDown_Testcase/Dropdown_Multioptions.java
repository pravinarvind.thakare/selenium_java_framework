package DropDown_Testcase;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import Common_functions.Different_UI_functions_repositery;
import Common_functions.InstantiateBrowser_LaunchWebsite;

public class Dropdown_Multioptions extends Different_UI_functions_repositery {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = instantiateDriver("Chrome");
		launchWebsite(driver, "https://www.amazon.in/");

//		searchBoxSendText(driver, "//input[@id=\"twotabsearchtextbox\"]", "mobile", 5);
//		List<WebElement> elements = get_elements(driver,
//				"(//div[@class=\"left-pane-results-container\"]/div)/div[@class=\"s-suggestion-container\"]/div/span",
//				5);
//
//		System.out.println(elements);
//		int count = elements.size();
//		System.out.println(count);
//
//		for (int i = 0; i < count; i++) {
//			String option_val = elements.get(i).getText();
//			System.out.println(option_val);
//
//			if (option_val.equals("stand")){
//				elements.get(i).click();
//				break;
//			}
//
//		}
		String dd_options = "(//div[@class=\"left-pane-results-container\"]/div)/div[@class=\"s-suggestion-container\"]/div/span";
		dropDown_options(driver, "//input[@id=\"twotabsearchtextbox\"]", "mobile", dd_options, null, "stand", 5);

		driver.quit();

	}

}
