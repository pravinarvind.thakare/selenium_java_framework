package snapdeal_POM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import Common_functions.Different_UI_functions_repositery;

public class Make_payment_objects extends Different_UI_functions_repositery {

	WebDriver driver;

	public Make_payment_objects(WebDriver driver) {
		super();
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//*[@title=\"Snapdeal\"]")
	WebElement logo;

	public WebElement logo() {
		return logo;
	}

	@FindBy(xpath = "//input[@placeholder=\"Mobile Number/Email\"]")
	WebElement enter_mobileNo;

	public WebElement enterMobileNoBox() {
		return enter_mobileNo;
	}

	@FindBy(xpath = "//button[@id=\"login-continue\"]")
	WebElement continuebutton;

	public WebElement continueButton() {
		return continuebutton;
	}

	@FindBy(xpath = "//div[@id=\"verification-block\"]/input")
	WebElement otpbox_click;

	public WebElement otpBoxClick() {
		return otpbox_click;
	}

}
