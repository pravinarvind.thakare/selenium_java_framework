package snapdeal_POM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Home_Page_objects {

	WebDriver driver;

	public Home_Page_objects(WebDriver driver) {
		super();
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	@FindBy(xpath = "//button[@id=\"pushDenied\" and text()=\"NOT NOW\"]")
	WebElement popup;

	public WebElement popup() {
		return popup;
	}

	@FindBy(xpath = "//*[@id=\"sdHeader\"]/div[4]/div[2]/div/div[1]/a[1]/img")
	WebElement logo;

	public WebElement logo() {
		return logo;
	}

	@FindBy(xpath = "//i[@class=\"sd-icon sd-icon-user\"]")
	WebElement signInLogo;

	public WebElement signInLogo() {
		return signInLogo;
	}

	@FindBy(xpath = "//*[@id=\"sdHeader\"]/div[4]/div[2]/div/div[3]/div[3]/div/span[2]/i")
	WebElement cartlogo;

	public WebElement cartlogo() {
		return cartlogo;
	}

	@FindBy(xpath = "//*[@id=\"leftNavMenuRevamp\"]/div[1]/div[2]/ul/div")
	WebElement topcatagoryText;

	public WebElement topcatagoryText() {
		return topcatagoryText;
	}

	@FindBy(xpath = "//*[@id=\"leftNavMenuRevamp\"]/div[1]/div[2]/ul/li[6]")
	WebElement morecatagoryText;

	public WebElement moreCatagoryText() {
		return morecatagoryText;
	}

	@FindBy(xpath = "//*[@id=\"leftNavMenuRevamp\"]/div[1]/div[2]/ul/li[1]/a/span[2]")
	WebElement mensfashion;

	public WebElement mensFashion() {
		return mensfashion;
	}

	@FindBy(xpath = "//*[@id=\"content_wrapper\"]/section/section/div[1]/div[2]/i[1]")
	WebElement forwardswipebutton;

	public WebElement forwardSwipeButton() {
		return forwardswipebutton;
	}

	@FindBy(xpath = "//*[@id=\"content_wrapper\"]/section/section/div[1]/div[1]/i[1]")
	WebElement backwardswipebutton;

	public WebElement backwardSwipeButton() {
		return backwardswipebutton;
	}

}
