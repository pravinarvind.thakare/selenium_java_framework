package snapdeal_POM;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import Common_functions.Different_UI_functions_repositery;
import Common_functions.Utilities_Repositery;

public class Make_payment_objects2 extends Utilities_Repositery {

	WebDriver driver;
	Actions act;

	public Make_payment_objects2(WebDriver driver) {
		super();
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//*[@title=\"Snapdeal\"]")
	WebElement logo;

	@FindBy(xpath = "//input[@placeholder=\"Mobile Number/Email\"]")
	WebElement enter_mobileNo;

	@FindBy(xpath = "//button[@id=\"login-continue\"]")
	WebElement continuebutton;

	@FindBy(xpath = "//div[@id=\"verification-block\"]/input")
	WebElement otpbox_click;

//	webpage specific pom methods

//	take screenshot of payment page
	public void paymentPageScreenshot(File logdir) throws IOException {
		takeScreenShot(driver, logdir, "PaymentPage_LoginDemand");
	}

//	verify snapdeal logo on payment page
	public void logo() {
		boolean logostatus = logo.isDisplayed();
		Assert.assertEquals(logostatus, true);
		System.out.println("verified logo on payment page");
	}

//	click on enter mobile box
	public void clickMobileNoBox() {
		clickElement(driver, enter_mobileNo, 5);
	}

//	enter mobile no
	public void enterMobileNo() {
		act = new Actions(driver);
		act.moveToElement(enter_mobileNo).sendKeys("9766049264").build().perform();
	}

//	click on continue button
	public void continueButton() {
		clickElement(driver, continuebutton, 5);
	}

//	click on enter otp box
	public void otpBoxClick() {
		object_handling_element(driver, otpbox_click, 5);
	}

//	take screenshot
	public void screenshot(File logdir) throws IOException {
		takeScreenShot(driver, logdir, "OTP_verification");
	}
}
