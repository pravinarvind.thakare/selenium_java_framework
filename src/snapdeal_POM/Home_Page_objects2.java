package snapdeal_POM;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.AssertJUnit;

import Common_functions.Different_UI_functions_repositery;
import Common_functions.Utilities_Repositery;
import POM_testcase.Snapdeal_testcase_chrome;

public class Home_Page_objects2 extends Utilities_Repositery {

	WebDriver driver;

	public Home_Page_objects2(WebDriver driver) {
		super();
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	@FindBy(xpath = "//button[@id=\"pushDenied\" and text()=\"NOT NOW\"]")
	WebElement popup;

	@FindBy(xpath = "//img[contains(@class, \"notIeLogoHeader\")]")
	WebElement snapdeallogo;

	@FindBy(xpath = "//i[@class=\"sd-icon sd-icon-user\"]")
	WebElement signInLogo;

	@FindBy(xpath = "//i[@class=\"sd-icon sd-icon-cart-icon-white-2\"]")
	WebElement cartlogo;

	@FindBy(xpath = "//div[text()=\"Top Categories\"]")
	WebElement topcatagoryText;

	@FindBy(xpath = "//li[text()=\"More Categories\"]")
	WebElement morecatagoryText;

	@FindBy(xpath = "//*[@id=\"sdFooter\"]/div[@class=\"borderBottom\"]/div/a")
	WebElement scrollto;

	@FindBy(xpath = "//span[text()=\"Men's Fashion\"]")
	WebElement mensfashion;

	@FindBy(xpath = "//i[@class=\"prev-i\"]")
	WebElement forwardswipebutton;

	@FindBy(xpath = "//i[@class=\"next-i\"]")
	WebElement backwardswipebutton;
	
//	webpage specific pom methods

//  pop up handling
	public void popup() {
		object_handling_element(driver, popup, 5);
	}

	// verification of snapdeal logo
	public void logo() {
		boolean logo = snapdeallogo.isDisplayed();
		AssertJUnit.assertTrue(logo);
		System.out.println("logo varified");
	}

	// verification of sign in logo
	public void signInLogo() {
		boolean signin = signInLogo.isDisplayed();
		Assert.assertEquals(signin, true);
		System.out.println("SignInLogo varified");

	}

	// verification of cart logo
	public void cartlogo() {
		boolean cartLogo = cartlogo.isDisplayed();
		Assert.assertEquals(cartLogo, true);
		System.out.println("CartLogo varified");

	}

	// click on backward button

	public void forwardSwipeButton() {
		clickElement(driver, forwardswipebutton, 5);
		System.out.println("swipe forward button handled");
	}

	// click on forward button
	public void backwardSwipeButton() {
		clickElement(driver, backwardswipebutton, 5);
		System.out.println("swipe backward button handled");
	}

	// highlight more category text and print
	public void moreCatagoryText() {
		double_click_element(driver, morecatagoryText, 5);
		String morecatagorytext = getText_element(driver, morecatagoryText, 5);
		System.out.println(morecatagorytext);
	}

	// highlight top category text and print
	public void topCatagoryText() {
		double_click_element(driver, topcatagoryText, 5);
		System.out.println(getText_element(driver, topcatagoryText, 5));
	}

//	hover on men's fashion category tab
	public void mensFashion() {
		hoverTo_element(driver, mensfashion, 5);
	}

//	scroll to bottom hyperlinks to open
	public void scrollto() {
		scroll_element(driver, scrollto, 10);
	}

//	multi tab open
	public void multi_tab_open() {
		multi_tab_open(driver, "//div[@class=\"row upperContent-footer\"]/a/div", 5);
	}

	public void screenshot(File logdir) throws IOException {
		takeScreenShot(driver, logdir, "men's_fashion_catlog");
	}

//	iterate through different windows
	public void mutlitab(File evidence_file) throws IOException {
//		get parent window handle
		String parentwindow = driver.getWindowHandle();
		System.out.println("parent tab handle is:" + parentwindow);

//	fetch different hyperlink tabs
		Set<String> windows = driver.getWindowHandles();
		System.out.println(windows);
		int count = windows.size();
		System.out.println(count);
		Iterator<String> windowsitr = windows.iterator();

//	iterate throgh different hyperlink tabs
		while (windowsitr.hasNext()) {
			String childwindow = windowsitr.next();
			if (!parentwindow.equals(childwindow)) {
				driver.switchTo().window(childwindow);
				System.out.println(driver.getTitle());
				takeScreenShot(driver, evidence_file, pagename(driver.getTitle()));
				driver.close();
			}

		}
		driver.switchTo().window(parentwindow);
	}

}
