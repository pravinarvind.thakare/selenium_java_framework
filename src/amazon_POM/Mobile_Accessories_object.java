package amazon_POM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Common_functions.Different_UI_functions_repositery;

public class Mobile_Accessories_object extends Different_UI_functions_repositery {

	WebDriver driver;

	public Mobile_Accessories_object(WebDriver driver) {
		super();
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//a[@data-csa-c-slot-id=\"nav_cs_4\"]")
	WebElement mobilelink;

	@FindBy(xpath = "//span[text()=\"Mobile Accessories\"]")
	WebElement mobileaccesorries;

	public void mobile_accessorries() {

		clickElement(driver, mobilelink, 5);
		clickElement(driver, mobileaccesorries, 5);
	}

}
