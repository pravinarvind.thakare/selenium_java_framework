package Action_Class_TestCase;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.google.common.io.Files;

public class Action_Scroll_Opening_MultiWindow_Screenshot extends Common_functions.Utilities_Repositery {

	static String dirname;

	public static void main(String[] args) throws InterruptedException, IOException {

		WebDriver driver = instantiateDriver("Chrome");
		launchWebsite(driver, "https://www.geeksforgeeks.org/");

//	    method 1: direct method

//		Actions action = new Actions(driver);
//		JavascriptExecutor jse = (JavascriptExecutor) driver;
//		WebElement scrollElement = get_element(driver, "(//a[contains(@class,\"view_all_button\")])[3]", 5);
//		jse.executeScript("arguments[0].scrollIntoView(true);", scrollElement);
//
//		List<WebElement> HyperLinks = get_elements(driver, "(//div[@class=\"ant-row gfg_home_topic_blue\"])", 5);
//		int count = HyperLinks.size();
//		for (int i = 0; i < count; i++) {
//			action.moveToElement(HyperLinks.get(i)).keyDown(Keys.CONTROL).click().keyUp(Keys.CONTROL).build().perform();
//
//		}
//
//		String projectdir = System.getProperty("user.dir");
//		File directory = new File(
//				projectdir + "\\screenshots\\brokenlinksvalidation\\" + driver.getTitle().replaceAll("\\|", ""));
//		if (directory.exists()) {
//			System.out.println("directory exists");
//		} else {
//			directory.mkdir();
//		}
//		Set<String> tabs = driver.getWindowHandles();
//		Iterator<String> tabsiterator = tabs.iterator();
//		while (tabsiterator.hasNext()) {
//			driver.switchTo().window(tabsiterator.next());
//			System.out.println(driver.getTitle());
//			File screenshotsource = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
//			File screenshotfilename = new File(directory + "\\.png");
//			Files.copy(screenshotsource, screenshotfilename);
//		}

//		method 2: using common function

		scroll(driver, "(//a[contains(@class,\"view_all_button\")])[3]", 5);
		multi_tab_open(driver, "(//div[@class=\"ant-row gfg_home_topic_blue\"])", 5);

		File evidencescreenshot = createLogDirectory("brokenlinksvalidation");

		Set<String> tabs = driver.getWindowHandles();
		Iterator<String> tabsiterator = tabs.iterator();
		while (tabsiterator.hasNext()) {
			driver.switchTo().window(tabsiterator.next());
			System.out.println(driver.getTitle());
			takeScreenShot(driver, evidencescreenshot, pagename(driver.getTitle()));
		}

		driver.quit();

	}

}
