package Action_Class_TestCase;

import org.openqa.selenium.WebDriver;

import Common_functions.Different_UI_functions_repositery;

public class Action_uppercase_shiftKey extends Different_UI_functions_repositery {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = instantiateDriver("Chrome");
		launchWebsite(driver, "https://www.amazon.in/");

//		method 1:direct method
		/*
		 * Actions act = new Actions(driver); WebElement element =
		 * driver.findElement(By.xpath("//input[@id=\"twotabsearchtextbox\"]"));
		 * act.moveToElement(element).click().keyDown(Keys.SHIFT).sendKeys("mobile").
		 * keyUp(Keys.SHIFT).click()
		 * .keyDown(Keys.ENTER).keyUp(Keys.ENTER).build().perform();
		 * 
		 */

//		method 2:using common functions

		upper_case(driver, "//input[@id=\"twotabsearchtextbox\"]", "mobile", 5);
		driver.quit();
	}

}
