package Action_Class_TestCase;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import Common_functions.Different_UI_functions_repositery;

public class Action_Double_Click extends Different_UI_functions_repositery {

	public static void main(String[] args) throws InterruptedException {

		WebDriver driver = instantiateDriver("Chrome");
		launchWebsite(driver, "https://www.wikipedia.org/");

//		method 1:Direct method
		/*
		 * Actions action = new Actions(driver); WebElement element =
		 * get_element(driver, "//strong[text()=\"The Free Encyclopedia\"]", 4);
		 * action.moveToElement(element).doubleClick().build().perform();
		 */

//       method 2: using common functions

		String duobleclicktext = "//strong[text()=\"The Free Encyclopedia\"]";

		double_click(driver, duobleclicktext, 5);
		driver.quit();

	}

}
