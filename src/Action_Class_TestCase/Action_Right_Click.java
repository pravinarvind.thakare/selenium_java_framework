package Action_Class_TestCase;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import Common_functions.Different_UI_functions_repositery;;

public class Action_Right_Click extends Different_UI_functions_repositery {

	public static void main(String[] args) throws InterruptedException {

		WebDriver driver = instantiateDriver("Chrome");
		launchWebsite(driver, "https://www.geeksforgeeks.org/");

//		method 1:direct method
		/*
		 * clickButton(driver, "//button[text()=\"Miss Out\"]", 5); Actions action = new
		 * Actions(driver);
		 * 
		 * WebElement element = get_element(driver, "(//a[text()=\"Sign In\"])[1]", 5);
		 * action.moveToElement(element).contextClick().build().perform();
		 */

//		method 2:using common functions

//		String closebutton_path = "//button[text()=\"Miss Out\"]";
		String rightclick_path = "//li[@class=\"header-main__left-list-item p-relative\"]";

		right_click(driver, rightclick_path, 5);

		driver.quit();

	}

}
