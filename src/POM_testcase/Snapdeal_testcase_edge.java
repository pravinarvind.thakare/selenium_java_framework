package POM_testcase;

import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import Common_functions.Utilities_Repositery;
import snapdeal_POM.Home_Page_objects;
import snapdeal_POM.Home_Page_objects2;
import snapdeal_POM.Make_payment_objects;
import snapdeal_POM.Make_payment_objects2;
import snapdeal_POM.Product_page_objects;
import snapdeal_POM.Product_page_objects2;
import snapdeal_POM.Select_product_objects;
import snapdeal_POM.Select_product_objects2;

public class Snapdeal_testcase_edge extends Utilities_Repositery {

	File evidencelog;
	WebDriver driver;
	Actions act;
	JavascriptExecutor jse;

	public File screenshot() throws IOException {
		takeScreenShot(driver, evidencelog, "mensFashionss");
		return evidencelog;
	}

	@BeforeTest
	public void presetup() {

		evidencelog = createLogDirectory("SnapdealProject");
		driver = instantiateDriver("Edge");
		launchWebsite(driver, "https://www.snapdeal.com/");
		act = new Actions(driver);

	}

	@Test(priority = 0)
	public void home_page_execution() throws InterruptedException, IOException {

//		create object of home page pom class
		Home_Page_objects2 obj = new Home_Page_objects2(driver);

//		popup handling method
		obj.popup();

//		snapdeal logo verification method
		obj.logo();

//		signin logo verification method
		obj.signInLogo();

//		cart logo verification method
		obj.cartlogo();

//		backward arrow button click method
		obj.backwardSwipeButton();

//		forward arrow button click method
		obj.forwardSwipeButton();

//		highlight more category text and print method
		obj.moreCatagoryText();

//		highlight top category text and print method
		obj.topCatagoryText();

//		hover on men's fashion method
		obj.mensFashion();
		Thread.sleep(5000);

//      take screenshot of men's fashion hovered category
		obj.screenshot(evidencelog);

//		scroll to bottom hyperlinks method
		obj.scrollto();

//		open each hyperlink in new tab
		obj.multi_tab_open();

//		open hyperlinks in new tabs,take screenshot and close tabs
		obj.mutlitab(evidencelog);

	}

	@Test(priority = 1)
	public void productSearch_apge_execution() throws IOException, InterruptedException {

		Select_product_objects2 selProd_obj = new Select_product_objects2(driver);

//		scroll back to top method
		selProd_obj.toppanel();

//		click on search box and enter the keyword
		selProd_obj.searchboxclick();

//		click on search button
		selProd_obj.searchbutton();

//		verify and print after search notification text
		selProd_obj.aftersearchtext();

//		click on smart wearables hyper link
		selProd_obj.smartwearables_hylink();

//		enter the price amount in minimum price range box
		selProd_obj.minimumpricebox();

//		enter the price amount in maximum price range box
		selProd_obj.maximumpricebox();

//		click on go button box
		selProd_obj.goBox();

//		click on sort by button
		selProd_obj.sortByDropdown();

//		select second category
		selProd_obj.category2();

//		click on sort by button
		selProd_obj.sortByDropdown();

//		click on desired category
		selProd_obj.desiredCategory();

//		hover to desired product from product list
		selProd_obj.productImageHover();

//		take screenshot of hovered product image
		Thread.sleep(3000);
		selProd_obj.screenshot(evidencelog);

//		select and click on desired product
		selProd_obj.clickOnProduct();
		;

//		switch to next new tab opened
		selProd_obj.switchToProductTab();
	}

	@Test(priority = 2)
	public void product_page_execution() throws IOException, InterruptedException {

		Product_page_objects2 prodpage_obj = new Product_page_objects2(driver);

//		take screenshot of selected product on product page
		Thread.sleep(5000);
		prodpage_obj.screenshot(evidencelog);

//		hover on product image
		prodpage_obj.productImageZoom();

//		click on downword arrow
		prodpage_obj.downWordArrow();

//		get product detail text
		prodpage_obj.productDetailText();

//		double click on star ratings
		prodpage_obj.starRating();

//		fetch star rating of product in numbers
		prodpage_obj.ratingText();

//		fetch product price
		prodpage_obj.priceDigit();

//		scroll to item detail hyperlink
		prodpage_obj.itemDetailsHylink();

//		print item details
		prodpage_obj.itemDetailHylinkText();

//		scroll to top
		prodpage_obj.scrollToTop();

//		enter pincode in pincode check box
		prodpage_obj.pincodeBox();

//		click on check box
		prodpage_obj.checkButton();

//		click on buy now button
		prodpage_obj.buyNowButton();

	}

	@Test(priority = 3)
	public void payment_page_execution() throws IOException {

		Make_payment_objects2 payment_obj = new Make_payment_objects2(driver);
//		take screenshot of payment page
		payment_obj.paymentPageScreenshot(evidencelog);

//		verify snapdeal logo on payment page
		payment_obj.logo();

//		click on enter mobile box
		payment_obj.clickMobileNoBox();;

//		enter mobile no
		payment_obj.enterMobileNo();

//		click on continue button
		payment_obj.continueButton();

//		click on enter otp box
		payment_obj.otpBoxClick();

//		take screenshot
		payment_obj.screenshot(evidencelog);

	}

	@AfterTest
	public void teardwon() {
		driver.quit();
	}
}
