package POM_testcase;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import Common_functions.Utilities_Repositery;
import snapdeal_POM.Home_Page_objects;
import snapdeal_POM.Make_payment_objects;
import snapdeal_POM.Product_page_objects;
import snapdeal_POM.Select_product_objects;

public class Snapdeal_POM_testcase extends Utilities_Repositery {

	static File evidencelog;
	static WebDriver driver;
	static Actions act;
	static JavascriptExecutor jse;

	@BeforeTest
	public static void presetup() {

		evidencelog = createLogDirectory("SnapdealProject");
		driver = instantiateDriver("Chrome");
		act = new Actions(driver);

	}

	@Test(priority = 0)
	public static void home_page_execution() throws InterruptedException, IOException {

		Home_Page_objects HP_Obj = new Home_Page_objects(driver);
		launchWebsite(driver, "https://www.snapdeal.com/");

//        pop handling
		object_handling_element(driver, HP_Obj.popup(), 5);

		// verification of snapdeal logo
		boolean logo = HP_Obj.logo().isDisplayed();
		AssertJUnit.assertTrue(logo);
		System.out.println("logo varified");

		// verification of sign in logo
		boolean signinlogo = HP_Obj.signInLogo().isDisplayed();
		AssertJUnit.assertTrue(signinlogo);
		System.out.println("SignInLogo varified");

		// verification of cart logo
		boolean cartlogo = HP_Obj.cartlogo().isDisplayed();
		AssertJUnit.assertTrue(cartlogo);
		System.out.println("CartLogo varified");

		// swipe on adds backward and forward
		clickElement(driver, HP_Obj.forwardSwipeButton(), 5);
		clickElement(driver, HP_Obj.forwardSwipeButton(), 5);

		System.out.println("swipe button backward and forward handled");

		// highlight text and print
		double_click_element(driver, HP_Obj.moreCatagoryText(), 5);
		String morecatagorytext = getText_element(driver, HP_Obj.moreCatagoryText(), 5);
		System.out.println(morecatagorytext);

		Thread.sleep(5000);
		double_click_element(driver, HP_Obj.topcatagoryText(), 5);
		String topcatagorytext = getText_element(driver, HP_Obj.topcatagoryText(), 5);
		System.out.println(topcatagorytext);

		hoverTo_element(driver, HP_Obj.mensFashion(), 5);

		takeScreenShot(driver, evidencelog, "mensFashionss");
		Thread.sleep(5000);

		scroll(driver, "//*[@id=\"sdFooter\"]/div[1]/div/a[1]/div", 10);
		// Thread.sleep(5000);

		multi_tab_open(driver, "//div[@class=\"row upperContent-footer\"]/a/div", 5);
		String parentwindow = driver.getWindowHandle();
		System.out.println("parent tab handle is:" + parentwindow);

		Set<String> windows = driver.getWindowHandles();
		System.out.println(windows);
		int count = windows.size();
		System.out.println(count);
		Iterator<String> windowsitr = windows.iterator();
		while (windowsitr.hasNext()) {
			String childwindow = windowsitr.next();
			if (!parentwindow.equals(childwindow)) {
				driver.switchTo().window(childwindow);
				System.out.println(driver.getTitle());
				takeScreenShot(driver, evidencelog, pagename(driver.getTitle()));
				driver.close();
			}

		}
		driver.switchTo().window(parentwindow);

	}

	@Test(priority = 1)
	public static void productSearch_apge_execution() throws IOException, InterruptedException {

//		driver = instatiateDriver("Chrome");
//		launchWebsite(driver, "https://www.snapdeal.com/");
		act = new Actions(driver);

		Select_product_objects selProd_obj = new Select_product_objects(driver);

//		scroll back to top
		scroll_element(driver, selProd_obj.toppanel(), 10);

//		click on search box and enter the keyword
		searchBox_enterText_element(driver, selProd_obj.searchboxclick(), "smart watch", 5);

//		click on search button
		clickElement(driver, selProd_obj.searchbutton(), 5);

//		verify and print after search notification text
		System.out.println(getElement_element(driver, selProd_obj.aftersearchtext(), 5).getText());

//		click on smart wearables hyper link
		clickElement(driver, selProd_obj.smartwearables_hylink(), 5);

//		enter the price amount in minimum price range box
		act.moveToElement(selProd_obj.minimumpricebox()).doubleClick().keyDown(Keys.DELETE).keyUp(Keys.DELETE)

				.sendKeys("500").build().perform();
//		enter the price amount in maximum price range box
		act.moveToElement(selProd_obj.maximumpricebox()).doubleClick().keyDown(Keys.DELETE).keyUp(Keys.DELETE)
				.sendKeys("4000").build().perform();

//		click on go button box
		clickElement(driver, selProd_obj.goBox(), 5);

//		click on sort by button
		clickElement(driver, selProd_obj.sortByDropdown(), 5);

//		select second category
		clickElement(driver, selProd_obj.category2(), 5);

//		click on sort by button
		clickElement(driver, selProd_obj.sortByDropdown(), 5);

//		click on desired category
		clickElement(driver, selProd_obj.desiredCategory(), 5);

//		hover to desired product from product list
		hoverTo_element(driver, selProd_obj.productImageHover(), 5);

//		take screenshot of hovered product image
		Thread.sleep(3000);
		takeScreenShot(driver, evidencelog, "desired product");

//		select and click on desired product
		clickElement(driver, selProd_obj.productImageHover(), 5);

//		switch to next new tab opened
		Set<String> window = driver.getWindowHandles();
		Iterator<String> newtabs = window.iterator();
		while (newtabs.hasNext()) {
			driver.switchTo().window(newtabs.next());
		}

	}

	@Test(priority = 2)
	public static void product_page_execution() throws IOException {

		Product_page_objects prodpage_obj = new Product_page_objects(driver);
		jse = (JavascriptExecutor) driver;
		takeScreenShot(driver, evidencelog, "selected _product");

//		hover on product image
		hoverTo_element(driver, prodpage_obj.productImageZoom(), 5);

//		click on downword arrow
		object_handling_element(driver, prodpage_obj.downWordArrow(), 5);

//		get product detail text
		System.out.println(getText_element(driver, prodpage_obj.productDetailText(), 5));

//		double click on star ratings
		act.moveToElement(prodpage_obj.starRating()).doubleClick().build().perform();

//		fetch star rating of product in numbers
		System.out.println(getText_element(driver, prodpage_obj.starRating(), 5));

//		fetch product price
		System.out.println(getText_element(driver, prodpage_obj.priceDigit(), 5));

//		scroll to product detail hyperlink
		jse.executeScript("arguments[0].scrollIntoView(true);", prodpage_obj.itemDetailsHylink());

//		print item details
		System.out.println(getText_element(driver, prodpage_obj.itemDetailHylinkText(), 5));

//		scroll to top
		jse.executeScript("arguments[0].scrollIntoView(true);", prodpage_obj.productDetailText());

//		enter pincode in pincode check box
		act.moveToElement(prodpage_obj.pincodeBox()).click().sendKeys("413508").build().perform();

//		click on check box
		clickElement(driver, prodpage_obj.checkButton(), 5);

//		click on buy now button
		clickElement(driver, prodpage_obj.buyNowButton(), 0);

	}

	@Test(priority = 3)
	public static void payment_page_execution() throws IOException {

		Make_payment_objects payment_obj = new Make_payment_objects(driver);

		takeScreenShot(driver, evidencelog, "paymentPage_loginDemand");

//		verify snapdeal logo on payment page
		boolean logostatus = payment_obj.logo().isDisplayed();
		System.out.println(logostatus);
		Assert.assertEquals(logostatus, true);

//		click on enter mobile box
		clickElement(driver, payment_obj.enterMobileNoBox(), 5);

//		enter mobile no
		act.moveToElement(payment_obj.enterMobileNoBox()).sendKeys("9766049264").build().perform();

//		click on continue button
		clickElement(driver, payment_obj.continueButton(), 5);

//		click on enter otp box
		object_handling_element(driver, payment_obj.otpBoxClick(), 5);
		
//		take screenshot
		takeScreenShot(driver, evidencelog, "otp_verification");
		

	}

	@AfterTest
	public static void teardwon() {
		driver.quit();
	}
}
