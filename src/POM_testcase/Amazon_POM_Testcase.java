package POM_testcase;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import Common_functions.Utilities_Repositery;
import amazon_POM.Mobile_Accessories_object;

public class Amazon_POM_Testcase extends Utilities_Repositery {

	public static void main(String[] args) throws IOException {

		File evidencefile = createLogDirectory("amazonMobileAccesorries");
		WebDriver driver = instantiateDriver("Chrome");
		launchWebsite(driver, "https://www.amazon.in/");
		Mobile_Accessories_object obj = new Mobile_Accessories_object(driver);
		obj.mobile_accessorries();
		takeScreenShot(driver, evidencefile, pagename(driver.getTitle()));

	}

}
