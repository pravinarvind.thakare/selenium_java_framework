package instantiateWebBrowser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

public class InstantiateMicrosoftEdgeBrowserUsingInBuiltDriverManager {

	public static void main(String[] args) {

		// Web driver manager automatically detects the version of Browser and downloads
		// corresponding driver at location (C:\Users\ADMIN or user
		// folder\.cache\selenium) and sets the system
		// property

//		creat refernce of edgedriver class

		WebDriver driver = new EdgeDriver();

//		launch website of any choise

		driver.get("https://www.youtube.com/");
		driver.manage().window().maximize();

//		close the edgedriver

		driver.quit();
	}

}
