package instantiateWebBrowser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class InstantiateMicrosoftEdgeBrowserUsingExternalDriverManager {

	public static void main(String[] args) {

//		setup edgedriver using webdriver manager

		WebDriverManager.edgedriver().setup();

//		create refernce of edgedriver class

		WebDriver driver = new EdgeDriver();

//		launch website of any choise

		driver.get("https://www.facebook.com/");
		driver.manage().window().fullscreen();

//	    close edgedriver

		driver.quit();

	}

}
