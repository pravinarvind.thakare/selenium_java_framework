package instantiateWebBrowser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class InstantiateMozilFireFoxBrowserWithGivenLocation {

	public static void main(String[] args) {

//		give address of mozila firefoxdriver address to our program

		System.setProperty("webdriver.gecko.driver", "E:\\MSSquareClass\\WebDrivers\\MozilafireFox\\geckodriver.exe");

//		create reference of firefox class
		WebDriver driver = new FirefoxDriver();

//		launch website of any choise

		driver.get("https://www.goibibo.com/");
		driver.manage().window().minimize();

//		close the firefoxdriver

		driver.quit();
	}

}
