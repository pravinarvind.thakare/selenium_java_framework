package instantiateWebBrowser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class InstantiateChromeBrowserUsingExternalDriverManager {

	public static void main(String[] args) {

//		set up chromedriver using webdriver manager

		WebDriverManager.chromedriver().setup();

//		create refernce of chromedriver class

		WebDriver driver = new ChromeDriver();

//		launch website of any choise

		driver.get("https://www.facebook.com/");
		driver.manage().window().getPosition();

//		 close the chromedriver

		driver.quit();

	}

}
