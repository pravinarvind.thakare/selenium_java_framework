package instantiateWebBrowser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class InstantiateMozilaFireFoxBrowserFetchingProjectLocation {

	public static void main(String[] args) {

//		fetch currant project location

		String project_dir = System.getProperty("user.dir");

//		give address of firefoxdriver using system.setproperty to our program by fetching project directory using system.getproperty

		System.setProperty("webdriver.gecko.driver", project_dir + "\\drivers\\geckodriver.exe");

//		create refernce of firefoxdriver class

		WebDriver driver = new FirefoxDriver();

//		launch website of any choise

		driver.get("https://www.amazon.in/");
		driver.manage().window().minimize();

//		close the firefoxdriver

		driver.quit();
	}

}
