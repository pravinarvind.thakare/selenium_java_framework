package instantiateWebBrowser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class InstantiateMozilaFireFoxBrowserUsingInBuiltDriverManager {

	public static void main(String[] args) {

		// Web driver manager automatically detects the version of Browser and downloads
		// corresponding driver at location (C:\Users\ADMIN or user
		// folder\.cache\selenium) and sets the system
		// property

//		create refernce of fireoxdriver class

		WebDriver driver = new FirefoxDriver();

//		launch website of any choise

		driver.get("https://www.youtube.com/");
		driver.manage().window().minimize();

//		close the firefoxdriver

		driver.quit();

	}

}
