package instantiateWebBrowser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class InstantiateMozilaFireFoxBrowserUsingExternalDriverManager {

	public static void main(String[] args) {

//		set up firefoxdriver using webdrivermanager 

		WebDriverManager.firefoxdriver().setup();

//		create refernce of firefoxdriver class

		WebDriver driver = new FirefoxDriver();

//		launch website of any choise

		driver.get("https://www.facebook.com/");
		driver.manage().window().minimize();

//		close the firefoxdriver

		driver.quit();

	}

}
