package Calender_Date_Testcase;

import org.openqa.selenium.WebDriver;

import Common_functions.Different_UI_functions_repositery;


public class Selenium_Calender extends Different_UI_functions_repositery {

	public static void main(String[] args) throws InterruptedException {

		WebDriver driver = instantiateDriver("Chrome");
		launchWebsite(driver, "https://www.makemytrip.com/");

//		for departure
		click(driver, "//span[@class=\"lbl_input appendBottom10\" and text()=\"Departure\"]", 5);
		click(driver, "//div[contains(@class,\"DayPicker-Day--today\")]", 5);

//		for return
		String dateOptions_path="//div[contains(@class,\"DayPicker-Day\") and contains(@aria-label,\"May\") and @aria-disabled=\"false\"]/div/p[1]";
		String dateclick_path="//div[contains(@class,\"DayPicker-Day\") and contains(@aria-label,\"May\") and @aria-disabled=\"false\"]/div/p";
		calender_Date_options(driver, "//p[@data-cy=\"returnDefaultText\"]", dateOptions_path, dateclick_path,"20", 5);
		
		driver.quit();

	}

}
