package TestCase;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common_functions.Utilities_Repositery;

public class Rightclick_Action extends Utilities_Repositery {

	static WebDriver driver;
	static File evidencefile;

	@BeforeTest
	public static void presetup() {
		evidencefile = createLogDirectory("flipkartrightclick");
		driver = instantiateDriver("Chrome");
		launchWebsite(driver, "https://www.flipkart.com/");

	}

	@Test
	public static void executor() {
//		popup_handling(driver, null, 0);
		right_click(driver, "//input[@class=\"Pke_EE\"]", 5);
	}

	@AfterTest
	public static void evidencecreation() throws IOException {
		takeScreenShot(driver, evidencefile, pagename(driver.getTitle()));
	}

}
