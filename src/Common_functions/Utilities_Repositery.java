package Common_functions;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.google.common.io.Files;

public class Utilities_Repositery extends Different_UI_functions_repositery {

	public static File createLogDirectory(String dirname) {

		String projectdir = System.getProperty("user.dir");
		System.out.println("currant project directory is:" + projectdir);
		File directory = new File(projectdir + "\\screenshots\\" + dirname);
		if (directory.exists()) {
			System.out.println(directory + " directory already exists");
		} else {
			System.out.println(directory + " directory does not exits,henece creating it");
			directory.mkdir();
			System.out.println(directory + " directory created");
		}
		return directory;

	}

	public static String pagename(String input) {
		String title = input.replaceAll("\\|", "");
		return title;
	}

	public static void takeScreenShot(WebDriver driver, File logdir, String screenshotfilename) throws IOException {

		File screenshotsource = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		File screeshotfile = new File(logdir + "\\" + screenshotfilename + ".png");
		Files.copy(screenshotsource, screeshotfile);
	}

}
